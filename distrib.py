import os

result = [os.path.join(dp, f) for dp, dn, filenames in os.walk("data") for f in filenames if os.path.splitext(f)[1] == '.opb']

with open("machines.txt") as file:
    machines = [machine.rstrip() for machine in file.readlines()]

# i = 0
# while len(result) > 0:
#     knap = result.pop()[:-4]
#     victime = machines[i % len(machines)]
#     i+=1
#     cmd = f"""ssh lfainsin@{victime} -o "ConnectTimeout 3" -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" "cd 2A/RO/tp2/ && tmux new-session -d 'julia test.jl {knap}.opb > {knap}.stdout_jl'" """
#     print(cmd)
#     os.system(cmd)

# i = 0
# while len(result) > 0:
#     knap = result.pop()[:-4]
#     victime = machines[i % len(machines)]
#     i+=1
#     cmd = f"""ssh lfainsin@{victime} -o "ConnectTimeout 3" -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" "cd 2A/RO/tp2/ && tmux new-session -d 'glpsol --lp {knap}.lp -o {knap}.sol > {knap}.stdout_lp'" """
#     print(cmd)
#     os.system(cmd)

# for machine in machines:
#     cmd = f"""ssh lfainsin@{machine} -o "ConnectTimeout 3" -o "StrictHostKeyChecking no" -o "UserKnownHostsFile /dev/null" "tmux kill-server" """
#     print(cmd)
#     os.system(cmd)
